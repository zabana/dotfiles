# # Abbreviations
abbr -a gs  git status
abbr -a gco git checkout
abbr -a gc  git commit
abbr -a c   clear
abbr -a gl  git log --oneline --color --graph

# nnn specific variables

# export NNN_PLUG=
#
export NNN_PLUG='f:finder;o:fzopen;p:preview-tabbed;d:diffs;t:nmount;v:imgview'
export NNN_FCOLORS="0000E6310000000000000000"

# Aliases
alias 0x="ssh devops@0xfishr.xyz"

alias pods="kubectl get pod"
alias svc="kubectl get svc"
alias ing="kubectl get ingress"
alias lconf="kubectl get conf"
alias lsec="kubectl get secret"
alias kapp="kubectl apply -f"
alias kdel="kubectl delete -f"

alias vim="nvim"
alias vimcf="cd ~/.dotfiles/nvim && nvim ~/.dotfiles/nvim/init.vim"
alias mkdir="mkdir -vp"
alias dot="cd ~/.dotfiles"
alias dc="docker-compose"
alias psh="pipenv shell"
alias vs="open -a 'Visual Studio Code'"
alias shutdown="shutdown now"
alias restart="shutdown -r"
alias dpyc="find . -name '*.pyc' -delete"
alias clean="find . -name '*.sw*' -delete"
alias tmux="tmux -u"
alias activate="source env/bin/activate"
alias ptest="pytest -v -s --disable-warnings"
alias up="nmcli radio wifi on 2>/dev/null && nmcli connection up 'Livebox-C6F2'"
alias down="nmcli connection down 'Livebox-C6F2' && nmcli radio wifi off"
alias ts="sudo systemctl status tor"
alias rstor="sudo systemctl restart tor"
alias py="ipython"
alias smbmap="/usr/bin/smbmap.py"
alias enum="cd ~/ctf/tools/enum"
alias serve="python -m http.server"
alias at="alacritty-themes"
alias ls="exa"
alias ll="exa -l"
alias la="exa -lah"
alias cat="bat --style plain"
alias copy="xclip -selection clipboard"
alias tips="cd ~/ctf/notes && vim tips.md"
alias tdl="td l -i"
alias notes="cd $HOME/ctf/notes/"
alias pwncat="pwncat --config $HOME/.dotfiles/pwncat/pwncatrc"
alias conf="vim ~/.dotfiles/fish/config.fish"
alias n="nnn -e"

# Env vars
set -x GOPATH $HOME/gowork
set -x WORDLISTS $HOME/ctf/tools/wordlists
set -x PATH ~/.npm-global/bin $PATH
set -x PATH /usr/local/go/bin $PATH
set -x PATH $GOPATH/bin $PATH
set -x PATH $HOME/.local/bin $PATH
set -x PATH $HOME/.cargo/bin $PATH
set -x PATH /var/lib/snapd/snap/bin $PATH
set -x PATH $HOME/.gem/ruby/2.7.0/bin $PATH
set -x PATH (yarn global bin) $PATH
set -x EDITOR /usr/bin/vim
set -x TERMINAL alacritty
set -x LANG en_GB.UTF-8
set -x QUOTING_STYLE literal
set -x PYTHON_KEYRING_BACKEND keyring.backends.null.Keyring
set -x _JAVA_AWT_WM_NONREPARENTING 1
set -x  NNN_FIFO /tmp/nnn.fifo


# disable fish greeting
function fish_greeting
end

# Start X at login
if status is-login
    if test -z "$DISPLAY" -a $XDG_VTNR = 1
        exec startx -- -keeptty
    end
end

function foot
    acestream-launcher --player $argv[1] $argv[2] --engine "acestreamengine --client-console"
end

# create mardown files for each chapter of a book
# I'm writing notes on
function summary
    for x in (seq $argv[1])
        touch ch_$x.md
    end
end

# create a bare repo (server only)
function repo
    if count $argv > /dev/null
        git init --bare /srv/git/$argv.git
    else
        echo "[Error] - please provide a name for the repository."
    end
end


# create a tryhackme project
function thm
    set -l path /home/$USER/ctf/tryhackme/$argv[1]
    if count $argv > /dev/null
        if test -d $path
            # if directory exists, cd into it
            cd $path
        else
            # else create it along with the necessary files
            mkdir $path
            mkdir $path/scan
            mkdir $path/files
            touch $path/notes.md
            cd $path
        end
    else
        cd /home/$USER/ctf/tryhackme
    end
end


# create a hackthebox project
function htb
    set -l path /home/$USER/ctf/hackthebox/$argv[1]
    if count $argv > /dev/null
        if test -d $path
            # if directory exists, cd into it
            cd $path
        else
            # else create it along with the necessary files
            mkdir $path
            mkdir $path/scan
            mkdir $path/files
            touch $path/notes.md
            cd $path
        end
    else
        cd /home/$USER/ctf/hackthebox
    end
end



function vpn
    if count $argv > /dev/null
        # do the thing
        cd /home/zabana/ctf/creds
        if test $argv[1] = 'htb'
            sudo openvpn lab_fishr.ovpn
        else if test $argv[1] = 'thm'
            sudo openvpn fishr-eu-1.ovpn
        else
            echo "Unrecognized service. Expected either thm or htb"
        end
    else
        echo "You must provide a service (thm|htb)"
    end
end


function cpsh
    set shelldir $HOME/ctf/tools/shells
    set regex "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"

    if not ip a show tun0 > /dev/null
        echo "Could not extract IP address. Are you connected to the VPN ?"
        return 1
    end

    set vpn_ip (ip a show tun0 2>/dev/null | grep -E -o $regex)

    if count $argv > /dev/null

        if test $argv[1] = 'bash'
            set file $shelldir/shell.sh

        else if test $argv[1] = 'php'
            set file $shelldir/shell.php

        else if test $argv[1] = 'php-cmd'
            set file $shelldir/cmd.php

        else if test $argv[1] = 'php-magic'
            set file $shelldir/magic_cmd.php

        else if test $argv[1] = 'js'
            set file $shelldir/shell.js

        else if test $argv[1] = 'nc'
            set file $shelldir/netcat

        else if test $argv[1] = 'py'
            set file $shelldir/shell.py
        end

        cp $file .
        set newfile (basename $file)

        if test $argv[1] != 'php-cmd' -o $argv[1] != 'php-magic'
            sed -i -e "s/[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}/$vpn_ip/g" $newfile
        end

        clip $newfile

    else
        echo "you must provide a language (bash|nc|php|php-cmd|php-magic|js|py)"
    end
end

function kbtail
    kubectl logs -f -l app=$argv[1]
end

function clip

    if count $argv > /dev/null
        cat $argv[1] | xclip -selection clipboard
    else
        echo "Copy what fool ??"
    end
end

function keys
    # generate an SSH keypair to use in CTF challenges
    # TODO: don't error out if basename(pwd) is file
    set curdir (pwd)
    if not cd $curdir/files 2>/dev/null
        echo "$curdir/files does not exists. Exiting ..."
        return 1
    else
        ssh-keygen -f fishr -N "" && sed -e "s/\zabana@wintermute//"
        cat fishr.pub | xclip -selection clipboard
    end
end

function scan
    nmap -p- $IP -oN scan/initial.txt
end

function dscan
    set ports (cat --style plain scan/initial.txt | grep -E -o "([0-9])+\/(tcp|udp)" | cut -d '/' -f1 | tr '\n' ',' | rev | cut -c 2- | rev)
    nmap -sC -sV -p $ports $IP -v -oN scan/detailed.txt
end
