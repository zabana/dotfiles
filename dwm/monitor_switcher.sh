#!/bin/bash

intern=eDP1
extern=HDMI1
export DISPLAY=:0.0
export XAUTHORITY="/home/zabana/.Xauthority"

if xrandr | grep "$extern disconnected"; then
    xrandr --output "$extern" --off --output "$intern" --primary --auto
else
    xrandr --output "$extern" --mode 2048x1152 --dpi 100 --scale 1x1 --primary --output "$intern" --auto --right-of "$extern"
fi
