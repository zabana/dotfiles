local colorscheme = "dracula"

if colorscheme == "tokyonight" then
	vim.g.tokyonight_style = "night"
	vim.g.tokyonight_italic_functions = true
	vim.g.tokyonight_sidebars = { "qf", "vista_kind", "terminal", "packer" }
end

-- customize dracula color palette
vim.g.dracula_colors = {
	bg = "#201b2d",
	fg = "#F8F8F2",
	selection = "#44475A",
	comment = "#626483",
	red = "#ed4556",
	orange = "#FFB86C",
	yellow = "#e7de79",
	green = "#67e480",
	purple = "#988bc7",
	cyan = "#a1efe4",
	pink = "#ff79c6",
	bright_red = "#FF6E6E",
	bright_green = "#69FF94",
	bright_yellow = "#FFFFA5",
	bright_blue = "#D6ACFF",
	bright_magenta = "#FF92DF",
	bright_cyan = "#A4FFFF",
	bright_white = "#FFFFFF",
	menu = "#201b2d",
	visual = "#3E4452",
	gutter_fg = "#4B5263",
	nontext = "#3B4048",
}
-- show the '~' characters after the end of buffers
vim.g.dracula_show_end_of_buffer = false
-- use transparent background
vim.g.dracula_transparent_bg = false
-- set custom lualine background color
vim.g.dracula_lualine_bg_color = "#44475a"
-- set italic comment
vim.g.dracula_italic_comment = true

local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)

if not status_ok then
	vim.notify("colorscheme " .. colorscheme .. " not found!")
	return
end
