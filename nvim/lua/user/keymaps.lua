local opts = { noremap = true, silent = true }

local term_opts = { silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap

--Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-----------------
-- Normal Mode --
-----------------

-- Better window navigation
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

-- Use comma to switch between tabs
keymap("n", ",", "<C-W><C-W>", opts)

-- -- Navigate between tabs with shift tab
-- keymap("n", "<Tab>", ":tabnext<CR>", opts)

-- Toggle file explorer
keymap("n", "<leader>n", ":NvimTreeToggle<cr>", opts)
keymap("n", "<leader>m", ":NvimTreeFindFileToggle<cr>", opts)

-- -- Navigate buffers
keymap("n", "<Tab>", ":bnext<CR>", opts)
keymap("n", "<S-Tab>", ":bprevious<CR>", opts)

-- Source init.lua
keymap("n", "<leader>s", ":source $MYVIMRC<cr>", opts)

-- save easily using <space> w
keymap("n", "<leader>w", ":w!<cr>", opts)

-- quit easily using <space> q
keymap("n", "<leader>q", ":Bdelete!<cr>", opts)

--  Cancel a search with <space> /
keymap("n", "<leader>/", ":nohlsearch<cr>", opts)

--  Cancel a search with <space> /
keymap("n", "<leader>b", ":TSHighlightCapturesUnderCursor<cr>", opts)

-- Resize with arrows
keymap("n", "<C-Up>", ":resize +2<CR>", opts)
keymap("n", "<C-Down>", ":resize -2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Navigate buffers
keymap("n", "<S-l>", ":bnext<CR>", opts)
keymap("n", "<S-h>", ":bprevious<CR>", opts)

-- " Make Y behave like the other capitals (ie select till the end of the line)
keymap("n", "Y", "y$", opts)

-- " Keep it centered
keymap("n", "n", "nzzzv", opts)
keymap("n", "N", "Nzzzv", opts)
keymap("n", "J", "mzJ`z", opts)

-- " Easy insertion of a trailing ; or , from insert mode
keymap("n", ";;", "A;<Esc>", opts)

-- Disable this
keymap("n", "Q", "<nop>", opts)

-- Telescope related keymaps
keymap(
	"n",
	"<C-p>",
	"<cmd>lua require'telescope.builtin'.find_files(require('telescope.themes').get_dropdown({ previewer = false }))<cr>",
	opts
)
keymap("n", "<A-g>", "<cmd>Telescope live_grep<cr>", opts)

-----------------
-- Insert Mode --
----------------g-

-- Press jk fast to enter
keymap("i", "jk", "<ESC>", opts)
keymap("i", "kj", "<ESC>", opts)

-----------------
-- Visual Mode --
-----------------

-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- Move text up and down
keymap("v", "<A-j>", ":m .+1<CR>==", opts)
keymap("v", "<A-k>", ":m .-2<CR>==", opts)
keymap("v", "p", '"_dP', opts)

-- Maintain the cursor position when yanking a visual selection
-- http://ddrscott.github.io/blog/2016/yank-without-jank/
keymap("v", "y", "myy`y", opts)
keymap("v", "Y", "myY`y", opts)

-----------------------
-- Visual Block Mode --
-----------------------

-- Move text up and down
keymap("x", "J", ":move '>+1<CR>gv-gv", opts)
keymap("x", "K", ":move '<-2<CR>gv-gv", opts)
keymap("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)

-------------------
-- Terminal Mode --
-------------------

-- Better terminal navigation
keymap("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
keymap("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
keymap("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
keymap("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)
