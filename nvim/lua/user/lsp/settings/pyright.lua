return {
	settings = {

		python = {
			analysis = {
				typeCheckingMode = "off",
				diagnosticMode = "workspace",
			},
		},
	},
}
